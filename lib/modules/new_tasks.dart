// ignore_for_file: prefer_const_constructors,prefer_const_literals_to_create_immutables, avoid_print

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:udemy_todo/modules/empty_screen.dart';
import 'package:udemy_todo/shared/cubit/cubit.dart';
import 'package:udemy_todo/shared/cubit/states.dart';
import 'package:udemy_todo/shared/task_item.dart';

class NewTasksScreen extends StatelessWidget {
  const NewTasksScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AppCubit, AppStates>(
        listener: (ctx, state) {},
        builder: (ctx, state) {
          var tasks = AppCubit.get(context).newTasks;
          return tasks.isEmpty ? const EmptyScreen(icon: Icons.menu,) : ListView.separated(
              itemBuilder: (ctx, index) => TaskItem(task: tasks[index]),
              separatorBuilder: (ctx, index) => Container(
                  width: double.infinity, height: 1, color: Colors.grey[300]),
              itemCount: tasks.length);
        });
  }
}
