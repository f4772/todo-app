import 'package:flutter/material.dart';

class EmptyScreen extends StatelessWidget {
  final IconData icon;
  const EmptyScreen({Key? key, required this.icon}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(icon,size:50),
          const SizedBox(height:10),
          const Text(
            'No tasks yet, please add some tasks',
            style:TextStyle(fontSize:18,fontWeight:FontWeight.bold)
          )
        ],
      ),
    );
  }
}
