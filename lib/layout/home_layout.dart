// ignore_for_file: prefer_const_constructors,prefer_const_literals_to_create_immutables, avoid_print, use_key_in_widget_constructors, must_be_immutable

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:udemy_todo/shared/cubit/cubit.dart';
import 'package:udemy_todo/shared/cubit/states.dart';

class HomeLayout extends StatelessWidget {
  var scaffoldKey = GlobalKey<ScaffoldState>();
  var formKey = GlobalKey<FormState>();

  TextEditingController titleController = TextEditingController();
  TextEditingController timeController = TextEditingController();
  TextEditingController dateController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (ctx) => AppCubit()..createDatabase(),
      child: BlocConsumer<AppCubit, AppStates>(
          listener: (BuildContext context, AppStates state) {
        if (state is AppInsertDatabaseState) {
          Navigator.pop(context);
          titleController.clear();
          timeController.clear();
          dateController.clear();
        }
      }, builder: (BuildContext context, AppStates state) {
        AppCubit cubit = AppCubit.get(context);
        return Scaffold(
          key: scaffoldKey,
          floatingActionButton: FloatingActionButton(
            onPressed: () async {
              // insertToDatabase();
              if (cubit.isBottomSheetOpen) {
                if (formKey.currentState?.validate() ?? false) {
                  cubit.insertToDatabase(
                    titleController.text,
                    dateController.text,
                    timeController.text,
                  );
                }
              } else {
                scaffoldKey.currentState
                    ?.showBottomSheet(
                        (context) => Container(
                              color: Colors.grey[100],
                              child: Padding(
                                padding: const EdgeInsets.all(20.0),
                                child: Form(
                                  key: formKey,
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      TextFormField(
                                        controller: titleController,
                                        validator: (val) {
                                          if (val?.isEmpty ?? false) {
                                            return "Title must not be empty";
                                          }
                                        },
                                        decoration: InputDecoration(
                                          label: Text("Title"),
                                          border: OutlineInputBorder(),
                                        ),
                                      ),
                                      SizedBox(height: 15),
                                      TextFormField(
                                        controller: dateController,
                                        keyboardType: TextInputType.datetime,
                                        readOnly: true,
                                        onTap: () {
                                          showDatePicker(
                                            context: context,
                                            initialDate: DateTime.now(),
                                            firstDate: DateTime.now(),
                                            lastDate:
                                                DateTime.parse('2022-12-31'),
                                          ).then((value) {
                                            dateController.text =
                                                DateFormat.yMMMd().format(
                                                    value ?? DateTime.now());
                                          });
                                        },
                                        validator: (val) {
                                          if (val?.isEmpty ?? false) {
                                            return "Date must not be empty";
                                          }
                                        },
                                        decoration: InputDecoration(
                                          label: Text("Date"),
                                          border: OutlineInputBorder(),
                                        ),
                                      ),
                                      SizedBox(height: 15),
                                      TextFormField(
                                        controller: timeController,
                                        keyboardType: TextInputType.datetime,
                                        readOnly: true,
                                        onTap: () {
                                          showTimePicker(
                                            context: context,
                                            initialTime: TimeOfDay.now(),
                                          ).then((value) {
                                            timeController.text = value
                                                    ?.format(context)
                                                    .toString() ??
                                                '';
                                          });
                                        },
                                        validator: (val) {
                                          if (val?.isEmpty ?? false) {
                                            return "Time must not be empty";
                                          }
                                        },
                                        decoration: InputDecoration(
                                          label: Text("Time"),
                                          border: OutlineInputBorder(),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                        elevation: 20)
                    .closed
                    .then((v) {
                  cubit.changeBottomSheetState(isShown: false);
                });
                cubit.changeBottomSheetState(isShown: true);
              }
            },
            child: Icon(cubit.isBottomSheetOpen ? Icons.add : Icons.edit,
                color: Colors.white),
          ),
          bottomNavigationBar: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            currentIndex: cubit.currentIndex,
            onTap: (index) {
              cubit.chnageIndex(index);
            },
            items: [
              BottomNavigationBarItem(icon: Icon(Icons.menu), label: "Tasks"),
              BottomNavigationBarItem(
                  icon: Icon(Icons.check_circle_outline), label: "Done"),
              BottomNavigationBarItem(
                  icon: Icon(Icons.archive_outlined), label: "Archived"),
            ],
          ),
          appBar: AppBar(
            title: Text(
              cubit.titles[cubit.currentIndex],
            ),
          ),
          body: state is AppGeteDatabaseLoadingState
              ? CircularProgressIndicator()
              : cubit.screens[cubit.currentIndex],
        );
      }),
    );
  }
}
