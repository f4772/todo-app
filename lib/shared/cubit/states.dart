abstract class AppStates{}

class AppInitialState extends AppStates{}

class AppChangeBottomNavbarState extends AppStates{}

class AppCreateDatabaseState extends AppStates{}

class AppGeteDatabaseState extends AppStates{}

class AppGeteDatabaseLoadingState extends AppStates{}

class AppInsertDatabaseState extends AppStates{}

class AppBottomSheetState extends AppStates{}

class AppUpdateDatabaseState extends AppStates{}

class AppDeleteDatabaseState extends AppStates{}