// ignore_for_file: avoid_print

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sqflite/sqflite.dart';
import 'package:udemy_todo/modules/archived_tasks.dart';
import 'package:udemy_todo/modules/done_tasks.dart';
import 'package:udemy_todo/modules/new_tasks.dart';
import 'package:udemy_todo/shared/cubit/states.dart';

class AppCubit extends Cubit<AppStates> {
  AppCubit() : super(AppInitialState());
  late Database db;
  bool isBottomSheetOpen = false;
  static AppCubit get(context) => BlocProvider.of(context);
  List<Map> newTasks = [];
  List<Map> doneTasks = [];
  List<Map> archivedTasks = [];

  int currentIndex = 0;
  List<Widget> screens = [
    const NewTasksScreen(),
    const DoneTasksScreen(),
    const ArchivedTasksScreen(),
  ];

  List<String> titles = ["New Tasks", "Done Tasks", "Archived Tasks"];

  void chnageIndex(int index) {
    currentIndex = index;
    emit(AppChangeBottomNavbarState());
  }

  void createDatabase() {
    openDatabase(
      'todo.db',
      version: 1,
      onCreate: (Database db, int version) async {
        print("DB Created");
        db
            .execute(
              'CREATE TABLE tasks (id INTEGER PRIMARY KEY , title TEXT, date TEXT, time TEXT, status TEXT)',
            )
            .then((value) => print("Table created"))
            .catchError((onError) {
          print("Error creating table $onError");
        });
      },
      onOpen: (Database db) async {
        print("DB Open");
        getDataFromDB(db);
      },
    ).then((value) {
      db = value;
      emit(AppCreateDatabaseState());
    });
  }

  void insertToDatabase(String title, String date, String time) async {
    await db.transaction((txn) async {
      txn
          .rawInsert(
        'INSERT INTO tasks(title,date,time,status) VALUES("$title","$date","$time","new")',
      )
          .then(
        (value) {
          print("New task $value inserted");
          emit(AppInsertDatabaseState());
          getDataFromDB(db);
        },
      ).catchError(
        (onError) {
          print("Error adding new task $onError");
        },
      );
    });
  }

  void getDataFromDB(Database db) {
    newTasks = [];
    doneTasks = [];
    archivedTasks = [];
    emit(AppGeteDatabaseLoadingState());

    db.rawQuery('SELECT * FROM tasks').then((value) {
      value.forEach((t) {
        if (t['status'] == 'new') {
          newTasks.add(t);
        } else if (t['status'] == 'done') {
          doneTasks.add(t);
        } else {
          archivedTasks.add(t);
        }
      });
      emit(AppGeteDatabaseState());
    }).catchError(
      (onError) {
        print("Error gettingData $onError");
      },
    );
  }

  void updateData(int id, String status) async {
    db.rawUpdate('UPDATE tasks SET status = ? WHERE id = ?', [status, id]).then(
        (value) {
      emit(AppUpdateDatabaseState());
      getDataFromDB(db);
    }).catchError(
      (onError) {
        print("Error updating task $onError");
      },
    );
  }

  void deleteTask(int id) async {
    db.rawDelete('DELETE FROM tasks WHERE id = ?', [id]).then(
        (value) {
      emit(AppDeleteDatabaseState());
      getDataFromDB(db);
    }).catchError(
      (onError) {
        print("Error deleting task $onError");
      },
    );;
  }

  void changeBottomSheetState({required bool isShown}) {
    isBottomSheetOpen = isShown;
    emit(AppBottomSheetState());
  }
}
