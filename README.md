
# Todo app

  

A flutter Todo app used to save tasks.
You can add new tasks, set tasks to done, archive old tasks or delete tasks.

Used technologies:

 - Flutter
 - Bloc State Management
 - SQLite database

